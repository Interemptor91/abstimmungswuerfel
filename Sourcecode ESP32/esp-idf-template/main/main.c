#include "adxl345/adxl345.h"
#include <driver/gpio.h>
#include "esp32_digital_led_lib.h"
#include "esp_event_loop.h"
#include <esp_log.h>
#include "esp_sleep.h"
#include "esp_wifi.h"
#include "freertos/event_groups.h"
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include "i2c/i2c.h"
#include <math.h>
#include "mqtt_client.h"
#include "nvs_flash.h"
#include "nvs.h"
#include "rom/rtc.h"

// I2C pins for Accelerometer
#define SCL_PIN 22
#define SDA_PIN 21

// number of leds per side of the cube used for the countdown
#define LED_COUNTDOWN 5

// number of leds
#define ANZ_LEDS 20

// pin for driving the leds
#define NEOPIXEL_PIN 14

// color, in this case it's blue
#define FARBE pixelFromRGB(0, 0, 255)

// delay for the countdown
#define NEOPIXEL_COUNTDOWN_MS 1000

// wifi AP settings
#define SSID "mqtttest"
#define WIFIPW "12345678"

// sleep time
#define SLEEP_TIME 60*1000000 // 60sec

// wifi event group the bits for checking the connection status
static EventGroupHandle_t wifi_event_group;
const int CONNECTED_BIT = BIT0;
const int DISCONNECTED_BIT = BIT1;

// id of the last cycle, this will be stored to NVS to avoid double answering a question
char last_abfrage[2] = {'0', '\0'};

// handler for NVS
nvs_handle nvs_handler;

/* details for neopixels
 * this is used from a library example and only pin and number of leds was adapted
 */
strand_t STRANDS[] = {
  {.rmtChannel = 1, .gpioNum = NEOPIXEL_PIN, .ledType = LED_WS2812B_V3, .brightLimit = 32, .numPixels =  ANZ_LEDS,
   .pixels = NULL, ._stateVars = NULL},
};

// number of leds strips
int STRANDCNT = sizeof(STRANDS)/sizeof(STRANDS[0]);

/* configure GPIO for neopixels
 * this is used from a library example
 */
void gpioSetup(int gpioNum, int gpioMode, int gpioVal) {
    gpio_num_t gpioNumNative = (gpio_num_t)(gpioNum);
    gpio_mode_t gpioModeNative = (gpio_mode_t)(gpioMode);
    gpio_pad_select_gpio(gpioNumNative);
    gpio_set_direction(gpioNumNative, gpioModeNative);
    gpio_set_level(gpioNumNative, gpioVal);
}

/* led countdown */
void countdown(strand_t * pStrand, unsigned long delay_ms)
{
	uint8_t counter = LED_COUNTDOWN;
	while (counter > 0) {
		// loop through leds and configure the color according to the actual countdown value
		for (uint16_t i = LED_COUNTDOWN-counter; i < LED_COUNTDOWN; i++) {
			for(uint16_t j = 0; j < pStrand->numPixels; j+=LED_COUNTDOWN) {
				pStrand->pixels[i+j] = FARBE; //blau
			}
		}

		// update the leds to set the colors
		digitalLeds_updatePixels(pStrand);

		// wait for defined time
		vTaskDelay(delay_ms / portTICK_PERIOD_MS);
		counter--;

		// reset the array of the leds, needed for the countdown because some leds need to be off
		digitalLeds_resetPixels_countdown(pStrand);
	}

	// reset the array of the leds and update all leds to state off
	digitalLeds_resetPixels(pStrand);
}

/* setup neopixels and start led countdown */
void neopixel()
{
	// setup the gpio configuration and initialize the configured led strips, in case of error restart ESP
	gpioSetup(STRANDS->gpioNum, GPIO_MODE_OUTPUT, 0);
	if (digitalLeds_initStrands(STRANDS, STRANDCNT)) {
		ESP_LOGI("NEOPIXEL", "FAILURE");
		esp_restart();
	}

	// start the countdown
	strand_t * pStrand = &STRANDS[0];
	countdown(pStrand, NEOPIXEL_COUNTDOWN_MS);
}

/* convert value of accelerometer to negative value in case of negative sign bit */
static int16_t ConvertSign(int acc)
{
   return (acc <= INT16_MAX) ? acc : (acc - 65536);
}

/* wifi event handler */
esp_err_t wifi_event_handler(void *ctx, system_event_t *event)
{
	switch(event->event_id) {
		// wifi station started, connect to the configured wifi
	    case SYSTEM_EVENT_STA_START:
	    	ESP_ERROR_CHECK(esp_wifi_connect());
	    	ESP_LOGI("WIFI", "WIFI STARTED");
	        break;

	    // got the ip from the AP, set the connected bit, that main can continue
	    case SYSTEM_EVENT_STA_GOT_IP:
	    	xEventGroupSetBits(wifi_event_group, CONNECTED_BIT);
	    	ESP_LOGI("WIFI", "WIFI CONNECTED");
	        break;

	    // station disconnected from AP, clear connected bit and set disconnected bit that main can continue and put ESP to sleep
	    case SYSTEM_EVENT_STA_DISCONNECTED:
	    	xEventGroupClearBits(wifi_event_group, CONNECTED_BIT);
	    	xEventGroupSetBits(wifi_event_group, DISCONNECTED_BIT);
	    	ESP_LOGI("WIFI", "WIFI DISCONNECTED");
	        break;
	    default:
	        break;
	    }
	return ESP_OK;
}

/* mqtt event handler */
static esp_err_t mqtt_event_handler(esp_mqtt_event_handle_t event)
{
    esp_mqtt_client_handle_t client = event->client;
	char toSend[11] = {'\0'};
	uint8_t side;
    int msg_id, x = 0, y = 0, z = 0;
	int acc[3];

    switch (event->event_id) {
    	/* mqtt client is connected to the server,
    	 * subscribe to the topic /abfrage/, in case of error, disconnect wifi and put ESP to sleep
    	 */
        case MQTT_EVENT_CONNECTED:
            ESP_LOGI("MQTT", "MQTT_EVENT_CONNECTED");
            msg_id = esp_mqtt_client_subscribe(client, "/abfrage/", 0);
            if(msg_id == -1)
            {
            	ESP_LOGI("MQTT", "SUBSCRIBE ERROR");
            	ESP_ERROR_CHECK(esp_wifi_disconnect());
            }
            break;

        // mqtt client got disconnected from the server, disconnect wifi and put ESP to sleep
        case MQTT_EVENT_DISCONNECTED:
            ESP_LOGI("MQTT", "MQTT_EVENT_DISCONNECTED");
            ESP_ERROR_CHECK(esp_wifi_disconnect());
            break;

        // subscription to the topic was successful, wait for data to receive
        case MQTT_EVENT_SUBSCRIBED:
            ESP_LOGI("MQTT", "MQTT_EVENT_SUBSCRIBED");
            break;

        // client was unsubscribed from the topic
        case MQTT_EVENT_UNSUBSCRIBED:
            ESP_LOGI("MQTT", "MQTT_EVENT_UNSUBSCRIBED");
            break;

        // publication of a message was successful, then disconnect wifi and put ESP to sleep
        case MQTT_EVENT_PUBLISHED:
            ESP_LOGI("MQTT", "MQTT_EVENT_PUBLISHED");
            ESP_ERROR_CHECK(esp_wifi_disconnect());
            break;

        /* data received for the subscribed topic,
         * start the led countdown, get the accelerometer data and send it to the server
         */
        case MQTT_EVENT_DATA:
            ESP_LOGI("MQTT", "MQTT_EVENT_DATA RECEIVED");

            /* check if the stored id equals the actual id of the question,
             * if yes, disconnect the wifi and put esp to sleep,
             * else get the accelerometer data and send it to the server
             */
            if(last_abfrage[0] != event->data[0])
            {
            	ESP_LOGI("MQTT", "NEUE ABFRAGE ID");
            	last_abfrage[0] = event->data[0];

            	// start accelerometer
            	initAcc(SCL_PIN, SDA_PIN);
				ESP_LOGI("ADXL345","ACCELEROMETER STARTED");

				// start countdown with Neopixels
				neopixel();

				// loop 100 values and build average value to get a proper value for the side
				for(int i = 0; i < 100; i++) {
					vTaskDelay(33 / portTICK_RATE_MS);
					// get accelerometer data
					bool ok = getAccelerometerData(acc);
					if(ok != false)
					{
						/* check if int value out of twos-complement value is in range of uint16
						 * if it's not in range, disconnect wifi and put ESP to sleep
						 */
						if ((abs(acc[0]) <= UINT16_MAX) && (abs(acc[1]) <= UINT16_MAX)
								&& (abs(acc[2]) <= UINT16_MAX))
						{
							// convert value to negative value if needed
							int16_t x_conv, y_conv, z_conv;
							x_conv = ConvertSign(acc[0]);
							y_conv = ConvertSign(acc[1]);
							z_conv = ConvertSign(acc[2]);

							// convert raw values to milli g unit
							static const float CONVERT_RAW_TO_mg = 0.00390625 * 1000;
							x += (float) x_conv * CONVERT_RAW_TO_mg;
							y += (float) y_conv * CONVERT_RAW_TO_mg;
							z += (float) z_conv * CONVERT_RAW_TO_mg;
						}
						else
						{
						    ESP_LOGI("ADXL345", "INVALID: %d %d %d", acc[0], acc[1], acc[2]);
						    ESP_ERROR_CHECK(esp_wifi_disconnect());
						}
					}
				}

				// put Accelerometer to sleep
				sleepAcc();

				// build average value
				x = x / 100;
				y = y / 100;
				z = z / 100;

				// check x, y, z values and get side of cube
				if(x > 500)
				{
					side = 3;
				}
				else if(x < -500)
				{
					side = 4;
				}
				else if(y > 500)
				{
					side = 2;
				}
				else if(y < -500)
				{
					side = 5;
				}
				else if(z < -500)
				{
					side = 1;
				}
				else
				{
					side = 0;
				}

				ESP_LOGI("MQTT", "SIDE TO SEND: %u", side);

				// publish the side of the cube to the server
				sprintf(toSend, "%u", side);
				msg_id = esp_mqtt_client_publish(client, "/antwort/", toSend, 0, 0, 0);

				/* if publish was successful, store abfrage ID to NVS,
				 * if publish was not successful, do nothing and publish is tried again after deep sleep */
				if(msg_id != -1)
				{
					// copy abfrage id in case of successful publish
					ESP_ERROR_CHECK(nvs_set_str (nvs_handler, "abfrage", last_abfrage));
					ESP_LOGI("MQTT", "NEUE ABFRAGE ID STORED TO NVS");
				}
				else
				{
					ESP_LOGI("MQTT", "PUBLISH WAS NOT SUCCESSFUL");
					ESP_ERROR_CHECK(esp_wifi_disconnect());
				}
            }
            else
            {
            	// question was already answered, disconnect wifi and put ESP to sleep
            	ESP_LOGI("MQTT", "MQTT ALREADY ANSWERED QUESTION");
            	ESP_ERROR_CHECK(esp_wifi_disconnect());
            }
            break;

        // an error occured, disconnect from wifi and put ESP to sleep
        case MQTT_EVENT_ERROR:
            ESP_LOGI("MQTT", "MQTT_EVENT_ERROR");
            ESP_ERROR_CHECK(esp_wifi_disconnect());
            break;
    }
    return ESP_OK;
}

/* main */
void app_main() {

	// Initialize NVS
	esp_err_t err = nvs_flash_init();
	if (err == ESP_ERR_NVS_NO_FREE_PAGES) {
		// NVS partition was truncated and needs to be erased
		// Retry nvs_flash_init
		ESP_ERROR_CHECK(nvs_flash_erase());
		err = nvs_flash_init();
	}
	// Check, if error occured during nvs_flash_init
	ESP_ERROR_CHECK( err );

	// open NVS with handler and check for error
	err = nvs_open("storage", NVS_READWRITE, &nvs_handler);
	if (err != ESP_OK)
	{
		ESP_LOGI("MAIN", "NVS OPEN FAILURE");
	}
	// no error occured
	else
	{
		last_abfrage[0] = '0'; // value will default to 0, if not set yet in NVS or on power on reset
		last_abfrage[1] = '\0';

		// if other reset than deep sleep wakeup store default value of last_abfrage to NVS
		if(rtc_get_reset_reason(0) != DEEPSLEEP_RESET) {
			ESP_ERROR_CHECK(nvs_set_str (nvs_handler, "abfrage", last_abfrage));
		}

		// get stored value of last_abfrage from NVS and check for error
		size_t required_size = sizeof(last_abfrage);
		err = nvs_get_str(nvs_handler, "abfrage", last_abfrage, &required_size);
		switch (err) {
			case ESP_OK:
				ESP_LOGI("MAIN", "NVS READ SUCCESS");
				break;
			case ESP_ERR_NVS_NOT_FOUND:
				ESP_LOGI("MAIN", "NVS READ VALUE NOT FOUND");
				break;
			default :
				ESP_LOGI("MAIN", "NVS READ FAILURE");
		}
	}

	// initialise and start WIFI
	tcpip_adapter_init();
	// create wifi event group for state handling of wifi connection and check for error
    wifi_event_group = xEventGroupCreate();
    if(wifi_event_group == NULL)
    {
    	ESP_LOGI("MAIN", "WIFI EVENT GROUP CREATE FAILURE");
    	esp_restart();
    }
    // initialize the event handler for the wifi event group
    ESP_ERROR_CHECK( esp_event_loop_init(wifi_event_handler, NULL) );

    /* configure the wifi connection
     * WIFI_STORAGE_RAM -> configuration will be stored in RAM
     * WIFI_MODE_STA -> Wifi station mode, so ESP32 will not be an access point
     * wifi_config_t stat_config -> configuration of AP to connect to
     */
    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK( esp_wifi_init(&cfg) );
    ESP_ERROR_CHECK( esp_wifi_set_storage(WIFI_STORAGE_RAM) );
    ESP_ERROR_CHECK( esp_wifi_set_mode(WIFI_MODE_STA) );
    wifi_config_t sta_config = {
        .sta = {
            .ssid = SSID,
            .password = WIFIPW,
        }
    };
    ESP_ERROR_CHECK( esp_wifi_set_config(WIFI_IF_STA, &sta_config) );
    ESP_ERROR_CHECK( esp_wifi_start() );

    // wait till wifi connected
    xEventGroupWaitBits(wifi_event_group,CONNECTED_BIT,false,true,portMAX_DELAY);

    /* initialise and start MQTT
     * uri -> ip and port of raspberry pi
     * event_handle -> function to handle the mqtt events
     * keepalive -> value in seconds to keep connection alive without any messages sent from server or client
     * disable_auto_reconnect -> true, because otherwise mqtt would reconnect in case of keepalive timeout.
     * 							 in this case the ESP should go to sleep instead of reconnecting
     */
	const esp_mqtt_client_config_t mqtt_cfg = {
		//.uri = "mqtt://test.mosquitto.org:1883",
		.uri = "mqtt://192.168.1.157:1883",
		.event_handle = mqtt_event_handler,
		.keepalive = 10,
		.disable_auto_reconnect = true,
	};
	esp_mqtt_client_handle_t client = esp_mqtt_client_init(&mqtt_cfg);
	if(client == NULL)
	{
		ESP_LOGI("MAIN", "MQTT CLIENT INIT FAILURE");
		esp_restart();
	}
	ESP_ERROR_CHECK(esp_mqtt_client_start(client));
	ESP_LOGI("MAIN", "MQTT CLIENT STARTED");

	// wait till wifi disconnected
	xEventGroupWaitBits(wifi_event_group,DISCONNECTED_BIT,false,true,portMAX_DELAY);

	// destroy the mqtt client and free the resources
	esp_mqtt_client_destroy(client);

	// stop wifi and delete the created event group
	ESP_ERROR_CHECK(esp_wifi_stop());
	vEventGroupDelete(wifi_event_group);

	// configure the sleep time and start deep sleep
	esp_sleep_enable_timer_wakeup(SLEEP_TIME);
	esp_deep_sleep_start();
}

