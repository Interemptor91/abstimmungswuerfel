deps_config := \
	/home/Alex/SDK/esp-idf/components/app_trace/Kconfig \
	/home/Alex/SDK/esp-idf/components/aws_iot/Kconfig \
	/home/Alex/SDK/esp-idf/components/bt/Kconfig \
	/home/Alex/SDK/esp-idf/components/driver/Kconfig \
	/home/Alex/SDK/esp-idf/components/esp32/Kconfig \
	/home/Alex/SDK/esp-idf/components/esp_adc_cal/Kconfig \
	/home/Alex/SDK/esp-idf/components/esp_http_client/Kconfig \
	/home/Alex/SDK/esp-idf/components/espmqtt/Kconfig \
	/home/Alex/SDK/esp-idf/components/ethernet/Kconfig \
	/home/Alex/SDK/esp-idf/components/fatfs/Kconfig \
	/home/Alex/SDK/esp-idf/components/freertos/Kconfig \
	/home/Alex/SDK/esp-idf/components/heap/Kconfig \
	/home/Alex/SDK/esp-idf/components/libsodium/Kconfig \
	/home/Alex/SDK/esp-idf/components/log/Kconfig \
	/home/Alex/SDK/esp-idf/components/lwip/Kconfig \
	/home/Alex/SDK/esp-idf/components/mbedtls/Kconfig \
	/home/Alex/SDK/esp-idf/components/openssl/Kconfig \
	/home/Alex/SDK/esp-idf/components/pthread/Kconfig \
	/home/Alex/SDK/esp-idf/components/spi_flash/Kconfig \
	/home/Alex/SDK/esp-idf/components/spiffs/Kconfig \
	/home/Alex/SDK/esp-idf/components/tcpip_adapter/Kconfig \
	/home/Alex/SDK/esp-idf/components/vfs/Kconfig \
	/home/Alex/SDK/esp-idf/components/wear_levelling/Kconfig \
	/home/Alex/SDK/esp-idf/components/bootloader/Kconfig.projbuild \
	/home/Alex/SDK/esp-idf/components/esptool_py/Kconfig.projbuild \
	/home/Alex/SDK/esp-idf/components/partition_table/Kconfig.projbuild \
	/home/Alex/SDK/esp-idf/Kconfig

include/config/auto.conf: \
	$(deps_config)


$(deps_config): ;
